const path = require('path')

const custom = require('../webpack.config.js')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions"
  ],
  "framework": "@storybook/vue",
  "core": {
    "builder": "@storybook/builder-webpack5"
  },
  webpackFinal: async (config) => {
    return {
      ...config,
      module: { ...config.module, rules: custom.module.rules },
      plugins: [ ...config.plugins, new MiniCssExtractPlugin() ],
      resolve: { ...config.resolve, alias: { ...config.resolve.alias, '@': path.resolve(__dirname, '../src') }}
    }
  }
}