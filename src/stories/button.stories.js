import MhButton from '../components/controls/button'

export default {
  title: 'Button',
  component: MhButton
}

const Template = (args, { argTypes }) => ({
  components: { MhButton },
  props: Object.keys(argTypes),
  template: '<mh-button v-bind="$props">Click Me</mh-button>'
})

export const Rounded = Template.bind({})

export const Bordered = Template.bind({})
Bordered.args = {
  type: 'bordered'
}

export const Squared = Template.bind({})
Squared.args = {
  type: 'squared'
}

export const Disabled = Template.bind({})
Disabled.args = {
  isDisabled: true
}
