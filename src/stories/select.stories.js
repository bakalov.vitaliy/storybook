import MhSelect from '../components/controls/select'

export default {
  title: 'Select',
  component: MhSelect,
  args: {
    options: [
      { value: 1, text: 'Option one' },
      { value: 2, text: 'Option two' },
      { value: 3, text: 'Option three' },
      { value: 4, text: 'Option four' }
    ]
  }
}

const Template = (args, { argTypes }) => ({
  components: { MhSelect },
  props: Object.keys(argTypes),
  template: '<mh-select v-bind="$props" />'
})

export const Regular = Template.bind({})

export const Disabled = Template.bind({})
Disabled.args = {
  isDisabled: true
}

export const NoOptions = Template.bind({})
NoOptions.args = {
  options: []
}
