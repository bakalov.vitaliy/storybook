import MhInput from '../components/controls/input'

export default {
  title: 'Input',
  component: MhInput
}

const Template = (args, { argTypes }) => ({
  components: { MhInput },
  props: Object.keys(argTypes),
  template: '<mh-input v-bind="$props" />'
})

export const Regular = Template.bind({})

export const Disabled = Template.bind({})
Disabled.args = {
  isDisabled: true
}

export const WithPlaceholder = Template.bind({})
WithPlaceholder.args = {
  placeholder: 'Enter text'
}
