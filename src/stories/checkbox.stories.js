import MhCheckbox from '../components/controls/checkbox'

export default {
  title: 'Checkbox',
  component: MhCheckbox,
  args: { label: 'Choose me' }
}

const Template = (args, { argTypes }) => ({
  components: { MhCheckbox },
  props: Object.keys(argTypes),
  template: '<mh-checkbox v-bind="$props" />'
})

export const Regular = Template.bind({})

export const Disabled = Template.bind({})
Disabled.args = {
  isDisabled: true
}
