export default {
  footer: {
    copyright: '© metalhead, 2022. All rights reserved'
  },

  page: {
    section: {
      button: {
        title: 'Button',
        text: 'Click me'
      },

      checkbox: {
        title: 'Checkbox',
        label: 'Choose me'
      },

      input: {
        title: 'Text field',
        placeholder: 'Enter text'
      },

      select: {
        title: 'Dropdown',
        option: {
          one: 'Option one',
          two: 'Option two',
          three: 'Option three',
          four: 'Option four'
        }
      }
    }
  }
}
