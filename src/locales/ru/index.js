export default {
  footer: {
    copyright: '© metalhead, 2022. Все права защищены'
  },

  page: {
    section: {
      button: {
        title: 'Кнопка',
        text: 'Кликни по мне'
      },

      checkbox: {
        title: 'Чекбокс',
        label: 'Выбери меня'
      },

      input: {
        title: 'Текстовое поле',
        placeholder: 'Введите текст'
      },

      select: {
        title: 'Выпадающий список',
        option: {
          one: 'Первая опция',
          two: 'Вторая опция',
          three: 'Третья опция',
          four: 'Четвертая опция'
        }
      }
    }
  }
}
